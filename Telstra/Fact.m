//
//  ViewController.m
//  Telstra
//
//  Created by Michael Williams on 10/11/2014.
//  Copyright (c) 2014 Michael Williams. All rights reserved.
//

#import "Fact.h"

@interface Fact ()

@end

@implementation Fact

@synthesize title;
@synthesize description;
@synthesize imageHref;

-(id) initWithParam: (NSString *) newTitle: (NSString *) newDescription: (NSString *) newImageHref
{
    if (self = [super init])
    {
        self.title = newTitle;
        self.description = newDescription;
        self.imageHref = newImageHref;
    }
    
    return self;
}

+ (NSMutableArray*) FactsWithJSON: (NSArray*) allResults;
{
    NSMutableArray *facts = [[NSMutableArray alloc] init];
    
    if (allResults.count > 0)
    {
        for (NSDictionary *fact in allResults)
        {
            NSString *newTitle = fact[@"title"];
            
            if ([[NSNull null] isEqual:newTitle]) {
                newTitle = @"No Title";
            }
            
            NSString *newDescription = fact[@"description"];
            
            if ([[NSNull null] isEqual:newDescription]) {
                newDescription = @"No Description";
            }
            
            NSString *newImageHref = fact[@"imageHref"];
            
            if ([[NSNull null] isEqual:newImageHref]) {
                newImageHref = @"nil";
            }
            
            Fact *newFact = [[[Fact alloc] initWithParam:newTitle:newDescription:newImageHref] retain];
            
            [facts addObject:newFact];
        }
    }
    
    return facts;
}

- (NSString*) getTitle
{
    return self.title;
}

- (NSString*) getDescription
{
    return self.description;
}

- (NSString*) getImageHref
{
    return self.imageHref;
}

@end