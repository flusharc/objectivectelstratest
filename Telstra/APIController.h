//
//  ViewController.h
//  Telstra
//
//  Created by Michael Williams on 10/11/2014.
//  Copyright (c) 2014 Michael Williams. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol APIControllerProtocol

- (void) didReceiveAPIResults:(NSDictionary *)results;

@end

@interface APIController : NSObject
{
    NSString *title;
    NSString *description;
    NSString *imageHref;
    
    __weak id <APIControllerProtocol> delegate;
}

@property (nonatomic, weak) id <APIControllerProtocol> delegate;

- (id) initWithDelegate:(id <APIControllerProtocol>) del;
- (void) retrieveFacts:(NSString *) path;

@end

