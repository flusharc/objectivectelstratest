//
//  ViewController.h
//  Telstra
//
//  Created by Michael Williams on 10/11/2014.
//  Copyright (c) 2014 Michael Williams. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "APIController.h"
#import "Fact.h"
#import "TableViewCell.h"

@interface TableViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, APIControllerProtocol>
{
    UITableView *factsTableView;
    
    NSString *kCellIdentifier;
    
    NSMutableArray *facts;
    NSDictionary *imageCache;
    APIController *api;
    
}

- (void) onContentSizeChange: (NSNotification *) notification;

@end

