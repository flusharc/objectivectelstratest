//
//  ViewController.h
//  Telstra
//
//  Created by Michael Williams on 10/11/2014.
//  Copyright (c) 2014 Michael Williams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCell : UITableViewCell
{
    bool layoutMade;
    
    UILabel *titleLabel;
    UILabel *descriptionLabel;
    UIImageView *thumbnailImageView;
}

- (void) setTitleText:(NSString *) newTitle;
- (void) setDecriptionText:(NSString *) newDescription;
- (void) setThumbnailImage:(NSString *) imageStr;
- (void) setThumbnailImageIMG:(UIImage *) img;
- (void) makeLayout;
- (void) setLayoutMade:(bool)made;

- (bool) getLayoutMade;

@end

