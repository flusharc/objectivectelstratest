//
//  ViewController.h
//  Telstra
//
//  Created by Michael Williams on 10/11/2014.
//  Copyright (c) 2014 Michael Williams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Fact : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *description;
@property (nonatomic, strong) NSString *imageHref;

- (id) initWithParam: (NSString *) newTitle: (NSString *) newDescription: (NSString *) newImageHref;
+ (NSMutableArray*) FactsWithJSON: (NSMutableArray*) allResults;
- (NSString*) getTitle;
- (NSString*) getDescription;
- (NSString*) getImageHref;

@end