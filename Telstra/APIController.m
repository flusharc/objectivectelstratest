//
//  ViewController.m
//  Telstra
//
//  Created by Michael Williams on 10/11/2014.
//  Copyright (c) 2014 Michael Williams. All rights reserved.
//

#import "APIController.h"

@interface APIController ()

@end

NSString *dropboxURL = @"https://dl.dropboxusercontent.com/u/746330/facts.json";

@implementation APIController

@synthesize delegate;

- (id) initWithDelegate:(id <APIControllerProtocol>) del
{
    if (self = [super init])
    {
        self.delegate = del;
    }
    
    return self;
}

- (void) retrieveFacts:(NSString *)path
{
    NSURL *url = [[NSURL URLWithString:dropboxURL] autorelease];
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:url
            completionHandler:^(NSData *data,
                                NSURLResponse *response,
                                NSError *error) {
                NSLog(@"Task Completed");
                
                if(error != nil) {
                    // If there is an error in the web request, print it to the console
                    NSLog(@"Error: %@", error.localizedDescription);
                }
                
                NSError *err;
                
                NSString *NSASCIIString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                
                NSData *convertedData = [NSASCIIString dataUsingEncoding:NSUTF8StringEncoding];
                
                id jsonData = [NSJSONSerialization JSONObjectWithData:convertedData options:NSJSONReadingMutableContainers error:&err];
                
                [NSASCIIString release];

                NSDictionary *results = jsonData;
                
                [self.delegate didReceiveAPIResults:results];
                
                [error release];
            }] resume];
}

@end
