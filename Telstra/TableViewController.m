//
//  ViewController.m
//  Telstra
//
//  Created by Michael Williams on 10/11/2014.
//  Copyright (c) 2014 Michael Williams. All rights reserved.
//

#import "TableViewController.h"

@interface TableViewController ()

@end

@implementation TableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // cell identifier for TableViewCell
    kCellIdentifier = @"CellIdentifier";
    
    // create new api object and set delegate to self
    api = [[[APIController alloc] initWithDelegate:self] retain];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = true;
    [api retrieveFacts:@"https://dl.dropboxusercontent.com/u/746330/facts.json"];
    
    self.navigationItem.title = @"Telstra";
    
    // add observer
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(titleChanged:) name:@"titleChangedNotification" object:nil];
    
    // add refresh button
    UIBarButtonItem *refreshBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshClicked:)];
    self.navigationItem.rightBarButtonItem = refreshBtn;
    [refreshBtn release];

    // setup facts table view
    factsTableView = [[UITableView alloc] init];
    [factsTableView setTranslatesAutoresizingMaskIntoConstraints:false];
    factsTableView.allowsSelection = false;
    factsTableView.delegate = self;
    factsTableView.dataSource = self;
    [factsTableView registerClass:[TableViewCell class] forCellReuseIdentifier:kCellIdentifier];
    // set this to whatever your "average" cell height is; it doesn't need to be very accurate
    factsTableView.estimatedRowHeight = 44.0;
    factsTableView.rowHeight = UITableViewAutomaticDimension;
    
    [self.view addSubview:factsTableView];
    
    // create view dictionary for laying out constraints
    NSDictionary *viewsDictionary = @{@"factsTableView":factsTableView};
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[factsTableView]|" options:0 metrics:nil views:viewsDictionary]];

    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[factsTableView]|" options:0 metrics:nil views:viewsDictionary]];
}

- (void) titleChanged:(NSNotification *) notification
{
    NSDictionary *userInfo = notification.userInfo;
    self.navigationItem.title = userInfo[@"title"];
    NSLog(@"Title changed to %@", userInfo[@"title"]);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return facts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier];

    if (cell == nil) {
        cell = [[[TableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:kCellIdentifier] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    Fact *fact = [facts objectAtIndex:indexPath.row];

    [cell setThumbnailImage:@"Blank52"];
    
    // Grab the imageHref key to get an image URL for the app's thumbnail
    NSString *urlString = fact.imageHref;
    
    // Check our image cache for the existing key. This is just a dictionary of UIImages
    UIImage *image = imageCache[urlString];
    
    if (!image)
    {
        if ([urlString rangeOfString:@"nil"].location == NSNotFound)
        // If the image does not exist, we need to download it, and check we have an image url
        {
            // Download an NSData representation of the image at the URL
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];
            
            [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                if (!error)
                {
                    UIImage *image = [UIImage imageWithData:data];
                    
                    // Store the image in to our cache
                    [imageCache setValue:image forKeyPath:urlString];
                    
                    // load image from cache
                    dispatch_async(dispatch_get_main_queue(), ^{
                        TableViewCell *cellToUpdate = (TableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
                        
                        if (cellToUpdate) {
                            [cellToUpdate setThumbnailImageIMG:image];
                        }
                    });
                }
                else {
                    NSLog(@"Error: - %@", error.localizedDescription);
                }
            }];
        }
    }
    else {
        // load image from cache
        dispatch_async(dispatch_get_main_queue(), ^{
            TableViewCell *cellToUpdate = (TableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
            
            if (cellToUpdate) {
                [cellToUpdate setThumbnailImageIMG:image];
            }
        });
    }

    if (![cell getLayoutMade])
        [cell makeLayout];
    
    [cell setTitleText:[fact getTitle]];
    [cell setDecriptionText:[fact getDescription]];
    
    return cell;
}

- (void) onContentSizeChange: (NSNotification *) notification
{
    [factsTableView reloadData];
}

// refresh function
- (IBAction)refreshClicked:(id)sender
{
    // refresh button removes table and fires another request for facts
    [facts removeAllObjects];
    [factsTableView reloadData];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = true;
    [api retrieveFacts:@"https://dl.dropboxusercontent.com/u/746330/facts.json"];
}

- (void) didReceiveAPIResults:(NSDictionary *)results
{
    NSDictionary *userInfo = @{@"title":[results[@"title"] retain]};

    // from deserialized json response deserialize objects into array and reload table
    [[NSNotificationCenter defaultCenter] postNotificationName:@"titleChangedNotification" object:userInfo];
    
    NSMutableArray *resultsArr = (NSMutableArray*)[results objectForKey:@"rows"];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        facts = [Fact FactsWithJSON:resultsArr];
        [factsTableView reloadData];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = false;
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

