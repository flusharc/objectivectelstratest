//
//  TableViewCell.m
//  Telstra
//
//  Created by Michael Williams on 10/11/2014.
//  Copyright (c) 2014 Michael Williams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TableViewCell.h"

@interface TableViewCell ()

@end

@implementation TableViewCell


-(id) init
{
    if (self = [super init])
    {
    }
    
    return self;
}

- (void) makeLayout
{
    // title label
    titleLabel = [[UILabel alloc] init];
    descriptionLabel = [[UILabel alloc] init];
    thumbnailImageView = [[UIImageView alloc] init];
    
    // title label, set translation to false to allow user-defined constraints
    [titleLabel setTranslatesAutoresizingMaskIntoConstraints:false];
    titleLabel.font = [UIFont boldSystemFontOfSize:16.0];
    titleLabel.numberOfLines = 0;
    
    // description label
    [descriptionLabel setTranslatesAutoresizingMaskIntoConstraints:false];
    descriptionLabel.font = [UIFont systemFontOfSize:14.0];
    descriptionLabel.numberOfLines = 0;

    // thumbnail image
    [thumbnailImageView setTranslatesAutoresizingMaskIntoConstraints:false];

    // add ui elements to content
    [self.contentView addSubview:titleLabel];
    [self.contentView addSubview:descriptionLabel];
    [self.contentView addSubview:thumbnailImageView];
    
    // views dictionary for layout constraints
    NSDictionary *viewsDictionary = @{@"title":titleLabel, @"body":descriptionLabel, @"image":thumbnailImageView};
    
    NSDictionary *metricsDictionary = @{@"imageDim":@100};
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-5-[image(imageDim)]-5-[title]-5-|" options:0 metrics:metricsDictionary views:viewsDictionary]];
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-5-[image(imageDim)]-5-[body]-5-|" options:0 metrics:metricsDictionary views:viewsDictionary]];
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[title]-5-[body]-(>=5)-|" options:0 metrics:metricsDictionary views:viewsDictionary]];

    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[image(imageDim)]-(>=5)-|" options:0 metrics:metricsDictionary views:viewsDictionary]];

    layoutMade = true;
}

- (void) setTitleText:(NSString *) newTitle
{
    titleLabel.text = newTitle;
}

- (void) setDecriptionText:(NSString *) newDescription
{
    descriptionLabel.text = newDescription;
}

- (void) setThumbnailImage:(NSString *) imageStr
{
    thumbnailImageView.image = [UIImage imageNamed: imageStr];
}

- (void) setThumbnailImageIMG:(UIImage *) img
{
    thumbnailImageView.image = img;
}

- (bool) getLayoutMade
{
    return layoutMade;
}

- (void) setLayoutMade:(bool)made
{
    layoutMade = made;
}

@end